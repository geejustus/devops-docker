FROM ubuntu:latest

RUN apt-get update
RUN apt-get install nginx -y

COPY pag_inicial_searchbar.html /var/www/html/

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]